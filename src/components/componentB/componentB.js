import React, {useCallback, useState} from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import './ComponentB.scss'
import Files from 'react-files'

const ComponentB = (props) => {
    const colors = ['green', 'red', 'pink', 'blue', 'orange', 'black']
    const [color, setColor] = useState('');
    const [text, setText] = useState('');

    const onChangeColor = useCallback((event) => {
        setColor(event.target.value)

    }, []);

    const onChangeText = event => {
        setText(event.target.value)
    }

    return (
        <Form className='input-form'>
            <Form.Group>
                <Form.Control
                    as="select"
                    custom
                    onChange={onChangeColor}
                    className="input-text"
                >
                    <option value="" disabled selected hidden>
                        Please Choose...
                    </option>
                    {colors.map((name, index) => (
                        <option key={index}>{name}</option>
                    ))}
                </Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Control className="input-text"
                              type="text"
                              placeholder="Enter text"
                              onChange={onChangeText}/>
            </Form.Group>
            <Form.Group>
                <Button className="add-button"
                        variant="warning"
                        onClick={() => props.add(color, text)}>
                    Add
                </Button>
            </Form.Group>
            <Form.Group>
                <Row style={{textAlign: 'center'}}>
                    <Col sm={6}>
                        <Files
                            className="files-dropzone"
                            onChange={file =>
                                props.importData(file[0])
                            }
                            onError={err => console.log(err)}
                            accepts={[".json"]}
                            multiple
                            maxFiles={3}
                            maxFileSize={10000000}
                            minFileSize={0}
                            clickable
                        >
                            <Button className="update-info"
                                    variant='success'
                                    style={{color: "black"}}>
                                Import
                            </Button>
                        </Files>
                    </Col>
                    <Col>
                        <Button className="update-info"
                                variant='success'
                                style={{color: "black"}}
                                onClick={props.export}>
                            Export
                        </Button>
                    </Col>
                </Row>
            </Form.Group>
        </Form>
    )
}

export default ComponentB;