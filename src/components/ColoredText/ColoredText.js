import React from "react";
import {Card} from "react-bootstrap";
import './ColoredText.scss'

const ColoredText = (props) => {
    return (
        <Card>
            <Card.Body style={{backgroundColor: props.color, textAlign: "center", fontSize: 18}}>
                <div className={'TextArea'}>
                    <b>{props.text}</b>
                </div>
                <div style={{textAlign: 'right'}}>
                    <button className='btn info' onClick={() => props.delete(props.index)}>
                        X
                    </button>
                </div>
            </Card.Body>
        </Card>
    )
}

export default ColoredText;