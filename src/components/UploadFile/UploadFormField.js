
import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import InputGroup from "react-bootstrap/InputGroup";
import FormFile from "react-bootstrap/FormFile";
import Spinner from "react-bootstrap/Spinner";
import Image from "react-bootstrap/Image";

class UploadFormField extends React.Component {
    constructor(props) {
        super(props);
        this.fileInputRef = React.createRef();
        this.onBrowse = this.onBrowse.bind(this);
        this.onFileChanged = this.onFileChanged.bind(this);
    }

    onBrowse() {
        this.fileInputRef.current.click();
    }

    onFileChanged(e) {
        const newFile = e.target.files ? e.target.files[0] : null;
        this.props.onFileChanged(newFile);
        this.fileInputRef.current.value = null;
    }

    render() {
        let uploadButton;
        const doUpload = this.props.fileName !== '';
        const hideClear = !doUpload || this.props.isUploading;

        if (this.props.isUploading) {
            uploadButton = <Spinner as="span" animation="border" size="sm"/>;
        } else {
            uploadButton = doUpload ? "Upload" : "Browse";
        }

        return (
            <Form.Group>
                <InputGroup>
                    <div className="position-relative m-0 p-0 border-0">
                        <Form.Control
                            type="text"
                            placeholder="File..."
                            value={ this.props.fileName }
                            readOnly
                            required
                        />
                        <button
                            className={(
                                "btn bg-transparent position-absolute" +
                                (hideClear ? ' d-none' : '')
                            )}
                            style={{ top: 0, right: 0, opacity: "0.5" }}
                            onClick={ () => this.onFileChanged({ target: { value: null } }) }
                        >
                            <Image
                                src="./img/clear.svg"
                                alt="clear"
                            />
                        </button>
                    </div>
                    <InputGroup.Append>
                        <Button
                            disabled={ this.props.isUploading }
                            onClick={ doUpload && this.props.onUploadStarted ? this.props.onUploadStarted : this.onBrowse }
                        >
                            { uploadButton }
                        </Button>
                    </InputGroup.Append>
                </InputGroup>
                <FormFile.Input
                    hidden
                    ref={ this.fileInputRef }
                    onChange={ this.onFileChanged }
                />
            </Form.Group>
        );
    }
}

UploadFormField.defaultProps = {
    fileName: '',
    isUploading: false,
    onFileChanged: () => {},
    onUploadStarted: () => {},
};

export default UploadFormField;