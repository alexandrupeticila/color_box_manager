import React, {Component} from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import validateColor from "validate-color";
import ComponentB from '../../components/componentB/componentB'
import ColoredText from '../../components/ColoredText/ColoredText'
import './Manager.scss'

class Manager extends Component {
    state = {
        cards: []
    }

    onClickX = (index) => {
        let updatedCards = this.state.cards;
        updatedCards.splice(index, 1)

        this.setState({
            cards: updatedCards
        });

    }


    onClickAdd = (color, text) => {

        let newCards = this.state.cards;
        newCards.push({
            color: color,
            text: text
        })

        this.setState({
            cards: newCards
        })
    }

    onSelectInputFile = (file) => {
        const reader = new FileReader();
        reader.readAsText(file)

        reader.onloadend = event => {
            const uploadedCards = JSON.parse(event.target.result);
            const allCards = [...this.state.cards, ...uploadedCards]
            allCards.forEach(el => el.color = el.color && validateColor(el.color) ? el.color : "transparent")
            this.setState({
                cards: allCards
            })
        }
    }

    onClickExport = () => {
        const fileData = JSON.stringify(this.state.cards);
        const blob = new Blob([fileData], {type: "text/plain"});
        const url = URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.download = `${"TextData"}.json`;
        link.href = url;
        link.click();
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <ComponentB add={this.onClickAdd} export={this.onClickExport}
                                    importData={this.onSelectInputFile}/>
                    </Col>
                    <Col>
                        {this.state.cards.map((card, index) => (
                            <ColoredText color={card.color}
                                         text={card.text}
                                         delete={this.onClickX}
                                         index={index}
                                         key={index}/>))}
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default Manager;