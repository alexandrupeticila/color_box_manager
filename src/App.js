import React from 'react';
import Manager from "./containers/manager/Manager";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
    return (
        <div className="App">
            <Manager/>
        </div>
    );
}

export default App;
